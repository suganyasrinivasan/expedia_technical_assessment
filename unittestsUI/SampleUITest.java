import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SampleUITest {

	/* Get in what browser the UI web page test should be done */
	public static WebDriver getDriver() throws IOException {
		Properties properties = new Properties();
		FileInputStream fs = null;
		fs = new FileInputStream("browser.properties");
		properties.load(fs);
		String browser = properties.getProperty("execute.in.browser");

		if (browser.equalsIgnoreCase("IE")) {
			System.setProperty("webdriver.browser.driver",
					properties.getProperty("path.browser.driver"));
			return new InternetExplorerDriver();
		} else if (browser.equalsIgnoreCase("CH")) {
			System.setProperty("webdriver.browser.driver",
					properties.getProperty("path.browser.driver"));
			return new ChromeDriver();
		} else {
			System.setProperty("webdriver.browser.driver",
					properties.getProperty("path.browser.driver"));
			return new FirefoxDriver();
		}
	}

	@Test(description = "simple UI test to load the page and validate the title of the page")
	public void simpleUITest() {
		WebDriver driver = null;
		;
		try {
			driver = SampleUITest.getDriver();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail();
		}
		/* Wait for driver to get intialized */
		driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		/* Open and wait for the page to load */
		driver.get("wishlist.html");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		/* Validate the title of the web page */
		Assert.assertEquals(driver.getTitle(), "Your WishList!!");

	}

}
