import com.expediaexe.accessmodifyUserInfo.AccessUpdateUserInfo;
import com.expediaexe.accessmodifyUserInfo.AccessUpdateUserInfoIntf;
import com.expediaexe.membermodifymapdata.AddPlaceorVoteinMap;
import com.expediaexe.membermodifymapdata.ModifyMapIntf;
import com.expediaexe.notifypopularplaces.NotifyPopPlacebyEmail;
import com.expediaexe.notifypopularplaces.NotifyUsersUpvotedIntf;

/*Simple straight forward flow demo for a sample usecase*/
public class SampleIntFlowtest {
	/* User adding a location to map and wishlist and upvoting for one location */
	public static void main(String args[]) {

		AccessUpdateUserInfoIntf userintf = new AccessUpdateUserInfo() {
		};
		/* Added these cities to wishlist */
		userintf.accessExistingUserInfo("xyz", "xyz@gmail.com", "Rome");
		userintf.accessExistingUserInfo("xyz", "xyz@gmail.com", "Toronto");
		userintf.accessExistingUserInfo("xyz", "xyz@gmail.com", "coloumbo");
		userintf.accessExistingUserInfo("xyz", "xyz@gmail.com", "Tokyo");
		userintf.accessExistingUserInfo("xyz", "xyz@gmail.com", "Melbourne");
		/* Adding new place to the map */

		ModifyMapIntf modifyintf = new AddPlaceorVoteinMap();
		try {
			modifyintf.addNewPlaceorVote("Very beautiful place", "India",
					"Tanjore", "10.78", "79.13", "xyz", "xyz@gmail.com");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(e.getMessage());
		}
		AccessUpdateUserInfoIntf infoint = new AccessUpdateUserInfo() {
		};
		infoint.setWillingtoVote(true);
		/* Vote for a place */
		infoint.accessExistingUserInfo("xyz", "xyz@gmail.com", "Tanjore");
		/*
		 * Upvote by another 4 memebers so that it becomes popular place so the
		 * xyz user is email notifed
		 */
		infoint.accessExistingUserInfo("xyz1", "xyz1@gmail.com", "Tanjore");
		infoint.accessExistingUserInfo("xyz2", "xyz2@gmail.com", "Tanjore");
		infoint.accessExistingUserInfo("xyz3", "xyz3@gmail.com", "Tanjore");
		infoint.accessExistingUserInfo("xyz4", "xyz4@gmail.com", "Tanjore");
		infoint.setWillingtoVote(false);
		NotifyUsersUpvotedIntf notifyInt = new NotifyPopPlacebyEmail();
		try {
			notifyInt.notifyUsersbyMail("xyz", "xyz@gamil.com", "Tanjore");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.err.println(e.getMessage());

		}

	}
}
