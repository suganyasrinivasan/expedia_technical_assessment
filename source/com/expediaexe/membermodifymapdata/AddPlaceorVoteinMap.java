package com.expediaexe.membermodifymapdata;

import com.expediaexe.accessmodifyUserInfo.AccessUpdateUserInfo;
import com.expediaexe.accessmodifyUserInfo.UserDetails;
import com.expediaexe.googlemapsengine.GoogleMapEngineintf;
import com.expediaexe.googlemapsengine.TableRowData;
import com.expediaexe.googlemapsengine.UpdateData;

;

public class AddPlaceorVoteinMap extends AccessUpdateUserInfo implements
		ModifyMapIntf {

	public void addNewPlaceorVote(String description, String Country,
			String place, String longitude, String latitude, String Username,
			String EmailId) throws Exception {
		if (isUniqueVote(Username, EmailId, place)) {
			GoogleMapEngineintf update = new UpdateData();
			update.updateMap(new TableRowData(Country, place, latitude,
					longitude, description, 1));
		} else {
			throw new Exception();
		}

	}

	@Override
	public boolean accessExistingUserInfo(String username, String mailId,
			String place) {
		if (username == null || mailId == null || place == null)
			return false;
		UserDetails details = null;
		if ((details = checkIfalreadyExistingUser(username, mailId)) != null) {

			if (details.getVotedList().contains(place)) {
				return false;
			}
		}
		return true;
	}

	public boolean isUniqueVote(String Username, String Emailid, String place) {

		return accessExistingUserInfo(Username, Emailid, place);
	}
}
