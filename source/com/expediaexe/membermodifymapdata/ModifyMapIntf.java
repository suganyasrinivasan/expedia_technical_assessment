package com.expediaexe.membermodifymapdata;

public interface ModifyMapIntf {
	public void addNewPlaceorVote(String description, String Country,
			String place, String longitude, String latitude, String userName,
			String emailId) throws Exception;
}
