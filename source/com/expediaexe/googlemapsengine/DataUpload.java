package com.expediaexe.googlemapsengine;

import com.google.api.client.http.InputStreamContent;
import com.google.api.services.mapsengine.model.Layer;
import com.google.api.services.mapsengine.model.Map;

import com.google.api.services.mapsengine.model.Permission;
import com.google.api.services.mapsengine.model.PermissionsBatchUpdateRequest;
import com.google.api.services.mapsengine.model.PermissionsBatchUpdateResponse;
import com.google.api.services.mapsengine.model.PublishResponse;
import com.google.api.services.mapsengine.model.Table;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

/**
 * upload local CSV and VRT files into a new Maps Engine table, creating a layer
 * and map in the process.
 */
public class DataUpload implements GoogleMapEngineintf {

	public synchronized void createMap() throws Exception {

		Table table = GooglemapParams.getInstance().getTable();

		uploadFile(table, GooglemapParams.getInstance().getCsvfile(),
				"text/csv");
		uploadFile(table, GooglemapParams.getInstance().getVrtfile(),
				"text/plain");

		Layer layer = GooglemapParams.getInstance().getLayer();
		publishLayer(layer);

		Map map = GooglemapParams.getInstance().getMap();

		publishMap(map);

		setPermissions(map);

	}

	/** Uploads the file data to the empty table. */
	private void uploadFile(Table table, String fileName, String contentType)
			throws IOException {
		// Load the file into a stream that we can send to the API
		File file = new File(fileName);
		InputStream fileInputStream = new BufferedInputStream(
				new FileInputStream(file));
		InputStreamContent contentStream = new InputStreamContent(contentType,
				fileInputStream);

		// Upload!
		GooglemapParams.getInstance().getEngine().tables().files()
				.insert(table.getId(), fileName, contentStream).execute();
	}

	/** Publishes the given Layer */
	private PublishResponse publishLayer(Layer layer) throws IOException {
		return GooglemapParams.getInstance().getEngine().layers()
				.publish(layer.getId()).execute();
	}

	/** Marks the provided map as "published", making it visible. */
	private PublishResponse publishMap(Map map) throws IOException {
		String processingStatus = null;

		// Initially the map will be in a 'processing' state and will return
		// '409 Conflict'
		// while processing is happening. Poll until it's ready.
		while (!"complete".equals(processingStatus)) {
			// Note that if you are using the Maps Engine API Wrapper there is
			// no need to sleep between
			// requests, as it will automatically retry any 'rate limit
			// exceeded' errors.
			processingStatus = GooglemapParams.getInstance().getEngine().maps()
					.get(map.getId()).execute().getProcessingStatus();
		}

		return GooglemapParams.getInstance().getEngine().maps()
				.publish(map.getId()).execute();
	}

	/** Makes the map publicly visible. */
	private PermissionsBatchUpdateResponse setPermissions(Map map)
			throws IOException {
		PermissionsBatchUpdateRequest request = new PermissionsBatchUpdateRequest()
				.setPermissions(Arrays.asList(new Permission().setId("anyone")
						.setRole("viewer")));

		return GooglemapParams.getInstance().getEngine().maps().permissions()
				.batchUpdate(map.getId(), request).execute();
	}

	
	public void updateMap(TableRowData data) throws Exception {
		// TODO Auto-generated method stub

	}

}
