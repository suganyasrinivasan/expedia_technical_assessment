package com.expediaexe.googlemapsengine;

import com.google.api.services.mapsengine.model.Feature;
import com.google.api.services.mapsengine.model.FeaturesBatchInsertRequest;
import com.google.api.services.mapsengine.model.FeaturesBatchPatchRequest;
import com.google.api.services.mapsengine.model.Layer;
import com.google.api.services.mapsengine.model.ProcessResponse;
import com.google.api.services.mapsengine.model.PublishResponse;
import com.google.api.services.mapsengine.model.Table;
import com.google.maps.clients.mapsengine.geojson.Point;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * updating an existing vector table on Google Maps Engine, by adding a new
 * place in the table. As well as update style rule to an existing layer if the
 * vote count increases
 * 
 * Required the same credentials . Required the ID of the layer created.
 */
public class UpdateData implements GoogleMapEngineintf {

	public void updateMap(TableRowData data) throws Exception {

		if (data == null)
			throw new Exception("Wrong data passed for Update");
		if (data.getCountry() == null || data.getPlace() == null)
			throw new Exception("Invalid data passed for Update");

		Layer layer = GooglemapParams.getInstance().getEngine().layers()
				.get(GooglemapParams.getInstance().getLayerid()).execute();

		Table table = GooglemapParams.getInstance().getEngine().tables()
				.get(layer.getDatasources().get(0).getId()).execute();
		if (table.get(data.getPlace()) == null) {
			insertFeature(table, data);
		} else {
			updateFeature(table, data);
		}
		processLayer(layer);
		publishLayer(layer);

	}

	/** Adds a new feature to a table. */
	private void insertFeature(Table table, TableRowData data)
			throws IOException {
		// Build the dictionary of feature properties
		Map<String, Object> properties = new HashMap<>();
		properties.put("PLACE", data.getPlace());
		properties.put("COUNTRY", data.getCountry());
		properties.put("DESCRIPTION", data.getDescription());
		properties.put("VOTE", 1);
		// Build the geometry. Note that this is using the Maps Engine API
		// Wrapper for simplicity
		Point point = new Point(Double.parseDouble(data.getLatitude()),
				Double.parseDouble(data.getLongitude()));

		// Build the feature by attaching properties
		Feature newFeature = point.asFeature(properties);

		FeaturesBatchInsertRequest insertRequest = new FeaturesBatchInsertRequest()
				.setFeatures(Arrays.asList(newFeature));

		GooglemapParams.getInstance().getEngine().tables().features()
				.batchInsert(table.getId(), insertRequest).execute();
	}

	/** Updates a feature. */
	private void updateFeature(Table table, TableRowData data)
			throws IOException {
		// Set the properties to patch
		Map<String, Object> properties = new HashMap<>();
		properties.put("PLACE", data.getPlace()); // Required: the primary key
													// value to
		// update
		// Increment the count of the vote by 1 if the record is already
		// existing
		properties.put("VOTE",
				Integer.parseInt(properties.get("VOTE").toString()) + 1);

		// Build the feature. We're not changing geometry so we can omit it.
		Feature updateFeature = new Feature().setProperties(properties);

		FeaturesBatchPatchRequest patchRequest = new FeaturesBatchPatchRequest()
				.setFeatures(Arrays.asList(updateFeature));

		GooglemapParams.getInstance().getEngine().tables().features()
				.batchPatch(table.getId(), patchRequest).execute();
	}

	/** Queues the layer for processing, triggering the style update. */
	private ProcessResponse processLayer(Layer layer) throws IOException {
		return GooglemapParams.getInstance().getEngine().layers()
				.process(layer.getId()).execute();
	}

	/** Publishes the layer, making it visible. */
	private PublishResponse publishLayer(Layer layer) throws IOException {
		return GooglemapParams.getInstance().getEngine().layers()
				.publish(layer.getId()).execute();
	}

	@Override
	public void createMap() throws Exception {
		// TODO Auto-generated method stub

	}

}
