package com.expediaexe.googlemapsengine;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.http.javanet.NetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.gson.GsonFactory;
import com.google.api.services.mapsengine.MapsEngine;
import com.google.api.services.mapsengine.MapsEngineScopes;
import com.google.api.services.mapsengine.model.Border;
import com.google.api.services.mapsengine.model.Color;
import com.google.api.services.mapsengine.model.Datasource;
import com.google.api.services.mapsengine.model.DisplayRule;
import com.google.api.services.mapsengine.model.Filter;
import com.google.api.services.mapsengine.model.IconStyle;
import com.google.api.services.mapsengine.model.Layer;
import com.google.api.services.mapsengine.model.Map;
import com.google.api.services.mapsengine.model.MapItem;
import com.google.api.services.mapsengine.model.MapLayer;
import com.google.api.services.mapsengine.model.PointStyle;
import com.google.api.services.mapsengine.model.ScaledShape;
import com.google.api.services.mapsengine.model.ScalingFunction;
import com.google.api.services.mapsengine.model.Schema;
import com.google.api.services.mapsengine.model.Table;
import com.google.api.services.mapsengine.model.VectorStyle;
import com.google.api.services.mapsengine.model.ZoomLevels;
import com.google.maps.clients.BackOffWhenRateLimitedRequestInitializer;
import com.google.maps.clients.HttpRequestInitializerPipeline;

public class GooglemapParams {

	private static final File CREDENTIAL_STORE = new File(
			System.getProperty("user.home"), ".credentials/mapsengine.json");

	private final HttpTransport httpTransport = new NetHttpTransport();
	private final JsonFactory jsonFactory = new GsonFactory();

	private static final String CLIENT_SECRETS_FILE = "client_secrets.json";
	private static final String APPLICATION_NAME = "Expedia/WishlistUpload";
	private static final Collection<String> SCOPES = Arrays
			.asList(MapsEngineScopes.MAPSENGINE);
	private static final GooglemapParams params = new GooglemapParams();
	private static final File CSVFILE = new File("places.csv");
	private static final File VRTFILE = new File("places.vrt");

	/*
	 * Implementing a singleton class since only one instance of the map engine
	 * should be available throughout the execution
	 */
	private GooglemapParams() {
	}

	public synchronized static GooglemapParams getInstance() {
		return params;
	}

	private static final String PROJECT_ID = "api-project-38540712219";

	/* Parameters for the map */
	private static final MapsEngine ENGINE = getInstance().createEngine();
	private static final Table TABLE = getInstance().createTable(
			PROJECT_ID,
			Arrays.asList(getInstance().getCsvfile(), getInstance()
					.getVrtfile()));
	private static final String TABLEID = TABLE.getId();
	private static final String DEFAULT_USER_ID = "default";
	private static final Layer LAYER = params.createLayer(TABLE);
	private static final String LAYERID = LAYER.getId();
	private static final Map MAP = getInstance().createMap(LAYER);
	private static final String MAPID = MAP.getId();
	private static final String MAPURL = String.format(
			"https://mapsengine.google.com/%s-4/mapview/?authuser=0", MAPID);

	public File getCredentialStore() {
		return CREDENTIAL_STORE;
	}

	public String getClientSecretsFile() {
		return CLIENT_SECRETS_FILE;
	}

	public String getProjectId() {
		return PROJECT_ID;
	}

	public HttpTransport getHttpTransport() {
		return httpTransport;
	}

	public JsonFactory getJsonFactory() {
		return jsonFactory;
	}

	public String getApplicationName() {
		return APPLICATION_NAME;
	}

	public Collection<String> getScopes() {
		return SCOPES;
	}

	public String getCsvfile() {
		return CSVFILE.getAbsolutePath();
	}

	public String getVrtfile() {
		return VRTFILE.getAbsolutePath();
	}

	public String getDefaultUserId() {
		return DEFAULT_USER_ID;
	}

	public Layer getLayer() {
		return LAYER;
	}

	public String getLayerid() {
		return LAYERID;
	}

	public Map getMap() {
		return MAP;
	}

	public String getMapid() {
		return MAPID;
	}

	/** Creates a layer using the table provided. */
	private Layer createLayer(Table table) {
		ZoomLevels allZoomLevels = new ZoomLevels().setMin(0).setMax(24);

		// Define a rule to capture vote >4 and style it as a scaled blue circle
		DisplayRule positiveGrowth = new DisplayRule()
				.setZoomLevels(allZoomLevels)
				.setPointOptions(
						new PointStyle()
								.setIcon(new IconStyle()
										.setScaledShape(
												new ScaledShape()
														.setShape("circle")
														.setFill(
																new Color()
																		.setColor(
																				"blue")
																		.setOpacity(
																				0.5))
														.setBorder(
																new Border()
																		.setColor(
																				"blue")
																		.setWidth(
																				1.0)))
										.setScalingFunction(
												new ScalingFunction()
														.setColumn("VOTE"))))
				.setFilters(
						Arrays.asList(new Filter().setColumn("VOTE")
								.setOperator(">").setValue(4)));

		// Define a rule to capture votes <4 and style it as a scaled red circle
		DisplayRule negativeGrowth = new DisplayRule()
				.setZoomLevels(allZoomLevels)
				.setPointOptions(
						new PointStyle()
								.setIcon(new IconStyle()
										.setScaledShape(
												new ScaledShape()
														.setShape("circle")
														.setFill(
																new Color()
																		.setColor(
																				"red")
																		.setOpacity(
																				0.5))
														.setBorder(
																new Border()
																		.setColor(
																				"red")
																		.setWidth(
																				1.0)))
										.setScalingFunction(
												new ScalingFunction()
														.setColumn("VOTE"))))
				.setFilters(
						Arrays.asList(new Filter().setColumn("VOTE")
								.setOperator("<").setValue(4)));

		VectorStyle style = new VectorStyle().setType("displayRule")
				.setDisplayRules(Arrays.asList(positiveGrowth, negativeGrowth));

		// Build a new layer using the styles defined above and render using the
		// supplied table.
		Layer newLayer = new Layer()
				.setLayerType("vector")
				.setName("Popular wishlist")
				.setProjectId(table.getProjectId())
				.setDatasources(
						Arrays.asList(new Datasource().setId(table.getId())))
				.setStyle(style);

		try {
			return ENGINE.layers().create(newLayer).setProcess(true) // flag
																		// that
																		// this
																		// layer
																		// should
																		// be
																		// processed
																		// immediately
					.execute();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

	/** Creates an empty table in your maps engine account. */
	private Table createTable(String projectId, List<String> fileNames) {
		// Note that we need a com.google.api.services.mapsengine.model.File,
		// not a java.io.File
		List<com.google.api.services.mapsengine.model.File> files = new ArrayList<>(
				fileNames.size());
		for (String fileName : fileNames) {
			files.add(new com.google.api.services.mapsengine.model.File()
					.setFilename(fileName));
		}

		// Build the table, including the minimal schema that defines 'COUNTRY'
		// as the primary key
		Table newTable = new Table().setName("Popular places to visit")
				.setDescription("wonderful places in world to visit")
				.setSchema(new Schema().setPrimaryKey("PLACE"))
				.setProjectId(projectId).setFiles(files)
				.setTags(Arrays.asList("Wonderful places", "WishList"));

		try {
			return ENGINE.tables().upload(newTable).execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	private MapsEngine createEngine() {
		System.out.println("Authorizing.");
		Credential credential = null;
		try {
			credential = GoogleMapAuthUtils
					.authorizeUser(httpTransport, jsonFactory, SCOPES);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("Authorization successful!");

		// Set up the required initializers to 1) authenticate the request and
		// 2) back off if we
		// start hitting the server too quickly.
		HttpRequestInitializer requestInitializers = new HttpRequestInitializerPipeline(
				Arrays.asList(credential,
						new BackOffWhenRateLimitedRequestInitializer()));

		// The MapsEngine object will be used to perform the requests.
		return new MapsEngine.Builder(httpTransport, jsonFactory,
				requestInitializers).setApplicationName(APPLICATION_NAME)
				.build();

	}

	/** Creates a map using the layer provided */
	private Map createMap(Layer layer) {
		Map newMap = new Map().setName("Popular places to visit- Map")
				.setProjectId(layer.getProjectId());

		MapLayer mapLayer = new MapLayer().setId(layer.getId()).setKey(
				"map1-layer1");

		List<MapItem> layers = new ArrayList<>();
		layers.add(mapLayer);

		newMap.setContents(layers);

		// Map processing is triggered automatically, so no need to set a flag
		// during creation.
		try {
			return ENGINE.maps().create(newMap).execute();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public MapsEngine getEngine() {
		return ENGINE;
	}

	public Table getTable() {
		return TABLE;
	}

	public String getTableid() {
		return TABLEID;
	}

	public static String getMapurl() {
		return MAPURL;
	}

}
