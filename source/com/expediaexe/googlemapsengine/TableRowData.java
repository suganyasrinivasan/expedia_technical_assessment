package com.expediaexe.googlemapsengine;

public class TableRowData {
	private String country ;
	private String place ;
	private String latitude;
	private String longitude;
	private String description;
	private Integer votes;

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getPlace() {
		return place;
	}

	public void setPlace(String place) {
		this.place = place;
	}

	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getVotes() {
		return votes;
	}

	public void setVotes(Integer votes) {
		this.votes = votes;
	}

	public TableRowData(String country, String place, String latitude,
			String longitude, String description, Integer votes) {
		super();
		this.country = country;
		this.place = place;
		this.latitude = latitude;
		this.longitude = longitude;
		this.description = description;
		this.votes = votes;
	}

}
