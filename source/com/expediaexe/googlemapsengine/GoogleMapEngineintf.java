package com.expediaexe.googlemapsengine;

public interface GoogleMapEngineintf {

	public void createMap() throws Exception;
	public void updateMap(TableRowData data) throws Exception;


}
