package com.expediaexe.notifypopularplaces;

public interface NotifyUsersUpvotedIntf {
	public void notifyUsersbyMail(String username, String mailId, String place)
			throws Exception;

}
