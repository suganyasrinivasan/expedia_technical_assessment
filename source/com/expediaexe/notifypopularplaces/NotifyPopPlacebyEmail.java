package com.expediaexe.notifypopularplaces;

import java.io.UnsupportedEncodingException;
import java.util.Date;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.expediaexe.accessmodifyUserInfo.*;

public class NotifyPopPlacebyEmail extends AccessUpdateUserInfo implements
		NotifyUsersUpvotedIntf {

	@Override
	public boolean accessExistingUserInfo(String username, String mailId,
			String place) {
		UserDetails details = null;
		if ((details = checkIfalreadyExistingUser(username, mailId)) != null) {

			if (details.getVotedList().contains(place)) {
				try {
					sendmail(mailId, place, username);
				} catch (UnsupportedEncodingException | MessagingException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					return false;
				}
				return true;
			}
		}
		return false;
	}

	public void notifyUsersbyMail(String username, String mailId, String place)
			throws Exception {
		if (username == null || mailId == null || place == null) {
			throw new Exception("Invalid data for user notification");
		}
		accessExistingUserInfo(username, mailId, place);
	}

	public void sendmail(String emailID, String Place, String username)
			throws MessagingException, UnsupportedEncodingException {
		Properties props = System.getProperties();

		props.put("mail.smtp.host", "smtp.expedia.com");

		Session session = Session.getInstance(props, null);
		MimeMessage msg = new MimeMessage(session);
		// set message headers
		msg.addHeader("Content-type", "text/HTML; charset=UTF-8");
		msg.addHeader("format", "flowed");
		msg.addHeader("Content-Transfer-Encoding", "8bit");

		msg.setFrom(new InternetAddress("no_reply@expedia.com", "NoReply"));

		msg.setReplyTo(InternetAddress.parse(emailID, false));

		msg.setSubject("Thanks for voting " + Place
				+ " as one of the best places!!!", "UTF-8");

		msg.setText(
				"Dear "
						+ username
						+ ", The place you upvoted is now one of the popular places listed in our webpage!!",
				"UTF-8");

		msg.setSentDate(new Date());

		msg.setRecipients(Message.RecipientType.TO,
				InternetAddress.parse(emailID, false));
		Transport.send(msg);

	}
}
