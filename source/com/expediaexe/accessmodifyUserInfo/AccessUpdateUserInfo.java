package com.expediaexe.accessmodifyUserInfo;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;


public abstract class AccessUpdateUserInfo implements AccessUpdateUserInfoIntf {

	public boolean willingtoVote = false;
	public static final File userStoreFile = new File("userdetails.ser");

	final static boolean Serialiser(UserDetails details) {
		ObjectOutputStream out = null;
		try {
			out = new ObjectOutputStream(new FileOutputStream(
					userStoreFile.getAbsolutePath()));
			out.writeObject(details);
			out.flush();
			return true;

		} catch (java.io.IOException e) {
			e.printStackTrace();
			return false;
		} finally {
			try {
				out.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	protected final UserDetails checkIfalreadyExistingUser(String username,
			String mailId) {
		if (username == null || mailId == null)
			return null;
		FileInputStream file = null;
		ObjectInputStream in = null;

		try {
			file = new FileInputStream(userStoreFile.getAbsolutePath());
			in = new ObjectInputStream(file);
			UserDetails userdetails;
			while ((userdetails = (UserDetails) in.readObject()) != null) {
				if (userdetails.userName.equals(username)
						&& userdetails.emailId.equals(mailId)) {
					return userdetails;
				}
			}
			return null;

		} catch (Exception ex) {
			ex.printStackTrace();
		} finally {
			try {
				in.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				file.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}
		return null;
	}

	/* To be obtained from user interaction */
	public boolean getWillingtoVote(String place) {
		return willingtoVote;
	}

	public void setWillingtoVote(boolean value) {
		willingtoVote = value;
	}

	public boolean accessExistingUserInfo(String username, String mailId,
			String place)  {
		if (username == null || mailId == null) {
			return false;
		}
		UserDetails details = null;
		if ((details = checkIfalreadyExistingUser(username, mailId)) == null) {
			if (getWillingtoVote(place))
				return AccessUpdateUserInfo.Serialiser(new UserDetails(
						username, mailId, place, place));
			else
				return AccessUpdateUserInfo.Serialiser(new UserDetails(
						username, mailId, place, null));

		} else {
			if (!details.wishList.contains(place)) {
				details.wishList.add(place);
			}

		}
		return true;
	}

}
