package com.expediaexe.accessmodifyUserInfo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class UserDetails implements Serializable {
	/**
	 * The user information is serialized and then deserialized to get the
	 * information back
	 */
	private static final long serialVersionUID = -6086938538759408526L;
	String userName;
	String emailId;
	List<String> wishList;
	List<String> votedList;

	public List<String> getVotedList() {
		return votedList;
	}

	public void setVotedList(String votedList) {
		this.votedList.add(votedList);
	}

	public UserDetails(String userName, String emailId, String wishList,
			String vote) {
		super();

		this.userName = userName;
		this.emailId = emailId;
		this.wishList = new ArrayList<String>();
		if (wishList != null)
			this.wishList.add(wishList);
		if (vote != null)
			this.votedList.add(vote);
	}

	public List<String> getWishList() {
		return wishList;
	}

	public void setWishList(String wishList) {
		this.wishList.add(wishList);
	}

}
