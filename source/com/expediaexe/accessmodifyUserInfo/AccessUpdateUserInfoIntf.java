package com.expediaexe.accessmodifyUserInfo;

public interface AccessUpdateUserInfoIntf {
	public boolean getWillingtoVote(String place);
    public void setWillingtoVote(boolean value);
	public boolean accessExistingUserInfo(String username, String mailId, String place);
}
