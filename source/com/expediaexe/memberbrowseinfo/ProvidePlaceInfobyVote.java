package com.expediaexe.memberbrowseinfo;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.expediaexe.googlemapsengine.GooglemapParams;
import com.expediaexe.googlemapsengine.TableRowData;

public class ProvidePlaceInfobyVote implements ProvidePlaceInfoIntf {

	private static List<TableRowData> listofPlaces = new ArrayList<TableRowData>();

	public List<TableRowData> getPlaceInfobyVotes(EnumOptionforPlaces value) {
		String line;
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(GooglemapParams
					.getInstance().getCsvfile()));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			while ((line = br.readLine()) != null) {

				String[] placedata = line.split(",");

				if (value.getValue() >= Integer.parseInt(placedata[3])) {
					listofPlaces.add(new TableRowData(placedata[1],
							placedata[2], placedata[4], placedata[5],
							placedata[0], Integer.parseInt(placedata[3])));
				}

			}
			return listofPlaces;

		} catch (NumberFormatException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		return null;

	}

	public String getPlaceInfobyDistance(int distance, String latitude,
			String longitude) {
		return null;
	}

	@Override
	public String getPlaceInfobyCountry( String country) {
		// TODO Auto-generated method stub
		return null;
	}
}
