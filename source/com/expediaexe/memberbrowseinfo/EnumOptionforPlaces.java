package com.expediaexe.memberbrowseinfo;

public enum EnumOptionforPlaces {
	MOSTPOPULAR(5), MEDIUMPOPULAR(3), LEASTPOULAR(1);

	private final int value;

	EnumOptionforPlaces(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
