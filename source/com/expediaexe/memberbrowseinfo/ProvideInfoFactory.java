package com.expediaexe.memberbrowseinfo;

public class ProvideInfoFactory {
	public ProvidePlaceInfoIntf getplaceInfobasedOnOption(EnumBrowseOptions info) {
		if (info.getValue() == EnumBrowseOptions.BYVOTE.getValue()) {
			return new ProvidePlaceInfobyVote();
		} else if (info.getValue() == EnumBrowseOptions.BYDISTANCE.getValue()) {
			return new ProvidePlaceInfobyDistance();
		} else if (info.getValue() == EnumBrowseOptions.BYCOUNTRY.getValue()) {
			return new ProvidePlaceInfobyCountry();
		}
		return null;
	}
}
