package com.expediaexe.memberbrowseinfo;

public enum EnumBrowseOptions {
	BYVOTE(0), BYDISTANCE(1), BYCOUNTRY(2);

	private final int value;

	EnumBrowseOptions(int value) {
		this.value = value;
	}

	public int getValue() {
		return value;
	}
}
