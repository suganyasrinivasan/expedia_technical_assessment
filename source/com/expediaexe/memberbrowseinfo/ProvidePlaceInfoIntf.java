package com.expediaexe.memberbrowseinfo;

import java.util.List;

import com.expediaexe.googlemapsengine.TableRowData;

public interface ProvidePlaceInfoIntf {
 
	public List<TableRowData> getPlaceInfobyVotes(EnumOptionforPlaces value);
	
	public String getPlaceInfobyDistance(int distance, String latitude,
			String longitude);
	public String getPlaceInfobyCountry(String country);
}
