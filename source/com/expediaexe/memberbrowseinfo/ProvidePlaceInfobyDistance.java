package com.expediaexe.memberbrowseinfo;

import java.io.IOException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;

import com.expediaexe.googlemapsengine.TableRowData;

public class ProvidePlaceInfobyDistance implements ProvidePlaceInfoIntf {

	public List<TableRowData> getPlaceInfobyVotes(EnumOptionforPlaces value) {
		return null;

	}

	public String getPlaceInfobyDistance(int distance, String latitude,
			String longitude) {
		HttpClient httpClient = HttpClientBuilder.create().build();
		/*
		 * Calling the google places map service to locate the places with given
		 * laitude and longitude and relative to distance
		 */

		if (distance <= 0 || latitude == null || longitude == null) {
			return null;
		}
		HttpGet getRequest = new HttpGet(
				"https://maps.googleapis.com/maps/api/place/nearbysearch/xml?location="
						+ latitude + "," + longitude + "&radius=" + distance
						+ "&key=AIzaSyBEzpWfJE60Jn0hhKX-x0EYBVY-PGt-1jA");
		org.apache.http.HttpResponse response = null;
		try {
			response = httpClient.execute(getRequest);
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HttpEntity entity = response.getEntity();
		String responseJsonString = null;
		try {
			responseJsonString = EntityUtils.toString(entity);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return responseJsonString;
	}

	@Override
	public String getPlaceInfobyCountry(String country) {
		// TODO Auto-generated method stub
		return null;
	}
}
