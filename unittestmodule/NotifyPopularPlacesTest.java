
import org.testng.Assert;
import org.testng.annotations.Test;

import com.expediaexe.notifypopularplaces.NotifyPopPlacebyEmail;
import com.expediaexe.notifypopularplaces.NotifyUsersUpvotedIntf;

public class NotifyPopularPlacesTest {
	@Test(description = "Successfully send mail", dependsOnMethods = { "" })
	public void notifyUserProperly() {
		NotifyUsersUpvotedIntf notify = new NotifyPopPlacebyEmail();
		try {
			notify.notifyUsersbyMail("abcd", "ABCD@gmail.com", "Sydney");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test(description = "Invalid data for notification", dependsOnMethods = { "" })
	public void notifyUserWrongData() {
		NotifyUsersUpvotedIntf notify = new NotifyPopPlacebyEmail();
		try {
			notify.notifyUsersbyMail(null, "ABCD@gmail.com", "Sydney");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Assert.fail();

	}

	@Test(description = "Invalid mail id for notification", dependsOnMethods = { "" })
	public void notifyUserMailException() {
		NotifyUsersUpvotedIntf notify = new NotifyPopPlacebyEmail();
		try {
			notify.notifyUsersbyMail("abcd", "ABCD$$junk", "Sydney");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail();
		}
	}

}
