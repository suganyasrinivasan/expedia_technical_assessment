
import junit.framework.Assert;

import org.testng.annotations.Test;

import com.expediaexe.accessmodifyUserInfo.AccessUpdateUserInfo;
import com.expediaexe.accessmodifyUserInfo.AccessUpdateUserInfoIntf;

public class AccessModifyUserTest {

	@Test(description = "Adding a new valid user with voting for place", dependsOnMethods = { "" })
	public void UpdateNewUserwithPlace() {

		AccessUpdateUserInfoIntf accessInt = new AccessUpdateUserInfo() {
		};
		accessInt.setWillingtoVote(true);
		Assert.assertEquals(accessInt.accessExistingUserInfo("abcd",
				"abcd@gmail.com", "chicogo"), true);

	}

	@Test(description = "Adding new user without place information without voting for place", dependsOnMethods = { "" })
	public void UpdateNewUserwithoutPlace() {
		AccessUpdateUserInfoIntf accessInt = new AccessUpdateUserInfo() {
		};
		accessInt.setWillingtoVote(false);

		Assert.assertEquals(accessInt.accessExistingUserInfo("efgh",
				"efgh@gmail.com", "seattle"), true);

	}

	@Test(description = "Updating already existing user with same city", dependsOnMethods = { "UpdateNewUserwithPlace" })
	public void UpdateoldusersamePlace() {
		AccessUpdateUserInfoIntf accessInt = new AccessUpdateUserInfo() {
		};
		Assert.assertEquals(accessInt.accessExistingUserInfo("abcd",
				"abcd@gmail.com", "chicogo"), true);

	}

	@Test(description = "Updating already existing user with different city", dependsOnMethods = { "UpdateNewUserwithPlace" })
	public void UpdateNewUserwithDiffPlace() {
		AccessUpdateUserInfoIntf accessInt = new AccessUpdateUserInfo() {
		};
		Assert.assertEquals(accessInt.accessExistingUserInfo("abcd",
				"abcd@gmail.com", "Sydney"), true);

	}

	@Test(description = "Updating with wrong information/invalid data", dependsOnMethods = { "" })
	public void UpdateNewUserwithInvalidData() {
		AccessUpdateUserInfoIntf accessInt = new AccessUpdateUserInfo() {
		};
		Assert.assertEquals(accessInt.accessExistingUserInfo(null,
				"wrong@gmail.com", "Sydney"), false);

	}

}
