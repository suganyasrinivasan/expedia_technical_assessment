
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeSuite;

import com.expediaexe.accessmodifyUserInfo.AccessUpdateUserInfo;

public class TestReadyConfig {

	// Before every test run the Users should be cleared for stable test results
	@BeforeSuite
	public void clearUsers() {
		Assert.assertEquals(true, AccessUpdateUserInfo.userStoreFile.delete());
		try {
			Assert.assertEquals(true,
					AccessUpdateUserInfo.userStoreFile.createNewFile());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail();
		}
	}

}
