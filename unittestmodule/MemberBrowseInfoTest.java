
import org.testng.Assert;
import org.testng.annotations.Test;

import com.expediaexe.memberbrowseinfo.EnumBrowseOptions;
import com.expediaexe.memberbrowseinfo.EnumOptionforPlaces;
import com.expediaexe.memberbrowseinfo.ProvideInfoFactory;
import com.expediaexe.memberbrowseinfo.ProvidePlaceInfoIntf;

public class MemberBrowseInfoTest {

	@Test(description = "Browse information by already existing votes", dependsOnMethods = { "" })
	public void checkBrowseInfobyPopularity() {
		ProvideInfoFactory info = new ProvideInfoFactory();
		ProvidePlaceInfoIntf infointf = info
				.getplaceInfobasedOnOption(EnumBrowseOptions.BYVOTE);
		Assert.assertNotNull(infointf);
		Assert.assertNotNull(infointf
				.getPlaceInfobyVotes(EnumOptionforPlaces.LEASTPOULAR));
	}

	@Test(description = "Browse by a given distance", dependsOnMethods = { "" })
	public void checkBrowseInfobyDistance() {
		ProvideInfoFactory info = new ProvideInfoFactory();
		ProvidePlaceInfoIntf infointf = info
				.getplaceInfobasedOnOption(EnumBrowseOptions.BYDISTANCE);
		Assert.assertNotNull(infointf);
		Assert.assertEquals(
				infointf.getPlaceInfobyDistance(10000, "-31.89", "105")
						.contains("200 OK"), true);
	}

	@Test(description = "Browse by a given country", dependsOnMethods = { "" })
	public void checkBrowseInfobyCountry() {
		ProvideInfoFactory info = new ProvideInfoFactory();
		ProvidePlaceInfoIntf infointf = info
				.getplaceInfobasedOnOption(EnumBrowseOptions.BYCOUNTRY);
		Assert.assertNotNull(infointf);
		Assert.assertEquals(
				infointf.getPlaceInfobyCountry("Ireland").contains("200 OK"),
				true);
	}

	@Test(description = "Browse by a invalid value for country", dependsOnMethods = { "" })
	public void checkBrowseInvalidCountry() {
		ProvideInfoFactory info = new ProvideInfoFactory();
		ProvidePlaceInfoIntf infointf = info
				.getplaceInfobasedOnOption(EnumBrowseOptions.BYCOUNTRY);
		Assert.assertNotNull(infointf);
		Assert.assertNull(infointf.getPlaceInfobyCountry(null));
	}

	@Test(description = "Browse by a invalid distance", dependsOnMethods = { "" })
	public void checkBrowseInfoworngDistance() {
		ProvideInfoFactory info = new ProvideInfoFactory();
		ProvidePlaceInfoIntf infointf = info
				.getplaceInfobasedOnOption(EnumBrowseOptions.BYDISTANCE);
		Assert.assertNotNull(infointf);
		Assert.assertNull(infointf.getPlaceInfobyDistance(0, null, null));

	}
}
