
import org.testng.Assert;
import org.testng.annotations.Test;

import com.expediaexe.googlemapsengine.DataUpload;
import com.expediaexe.googlemapsengine.GoogleMapEngineintf;
import com.expediaexe.googlemapsengine.TableRowData;
import com.expediaexe.googlemapsengine.UpdateData;

public class GoogleMapsUtilTest {
	@Test(description = "Test to create a new map and laod a layer and test", dependsOnMethods = { "" })
	public void createNewMap() {
		GoogleMapEngineintf mapintf = new DataUpload();
		try {
			mapintf.createMap();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail();

		}
	}

	@Test(description = "Update the existing map and test", dependsOnMethods = { "" })
	public void UpdateMapwithCorrectData() {
		GoogleMapEngineintf mapintf = new UpdateData();
		try {
			mapintf.updateMap(new TableRowData("India", "Mumbai", "19.01441",
					"72.8479385", "Beautiful commercial place", 1));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test(description = "Passing invalid data and check if it is throwing proper exception", dependsOnMethods = { "" })
	public void UpdateMapwithInvalidData() {
		GoogleMapEngineintf mapintf = new UpdateData();
		try {
			mapintf.updateMap(new TableRowData(null, null, "19.01441",
					"72.8479385", "Beautiful commercial place", 1));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.assertEquals(true,
					e.getMessage().equals("Invalid data passed for Update"));
		}
	}

	@Test(description = "Passing wrong data and check if it is throwing appropriate exception", dependsOnMethods = { "" })
	public void UpdateMapwitWrongData() {
		GoogleMapEngineintf mapintf = new UpdateData();
		try {
			mapintf.updateMap(null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.assertEquals(true,
					e.getMessage().equals("Wrong data passed for Update"));
		}
	}
}
