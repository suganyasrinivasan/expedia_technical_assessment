
import org.testng.Assert;
import org.testng.annotations.Test;

import com.expediaexe.membermodifymapdata.AddPlaceorVoteinMap;
import com.expediaexe.membermodifymapdata.ModifyMapIntf;

public class MemberModifyMapDataTest {

	@Test(description = "Add new location to the map and refresh the style", dependsOnMethods = { "" })
	public void modifyMapData() {
		ModifyMapIntf modifyint = new AddPlaceorVoteinMap();
		try {
			modifyint.addNewPlaceorVote("Nice place to hangout in week ends",
					"Italy", "Livorno", "43.542636", "10.3160048", "goodguy",
					"goodguy@gmail.com");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Assert.fail();
		}
	}

	@Test(description = "Adding same location which is duplicate.Should throw exception", dependsOnMethods = { "modifyMapData" })
	public void modifyMapDatanotUnique() {
		ModifyMapIntf modifyint = new AddPlaceorVoteinMap();
		try {
			modifyint.addNewPlaceorVote("Nice place to hangout in week ends",
					"Italy", "Livorno", "43.542636", "10.3160048", "goodguy",
					"goodguy@gmail.com");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		/* Fail the test case only if the exception is not thrown */
		Assert.fail();
	}

	@Test(description = "Adding Wrong data.Should throw exception", dependsOnMethods = { "" })
	public void modifyMapWrongData() {
		ModifyMapIntf modifyint = new AddPlaceorVoteinMap();
		try {
			modifyint.addNewPlaceorVote("Nice place to hangout in week ends",
					null, null, "43.542636", "10.3160048", "goodguy",
					"goodguy@gmail.com");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return;
		}
		/* Fail the test case only if the exception is not thrown */
		Assert.fail();
	}
}
